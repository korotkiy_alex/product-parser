<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'have_sitemap',
        'sitemap_url',
        'type_skan',
        'keywords',
        'regular_price_query',
        'price_file_url',
        'relation_file_url'
    ];

    public static function typesSkan()
    {
        return [
            1 => 'Sitemap',
            2 => 'XML-file',
            3 => 'Not skan',
            4 => 'Without sitemap',
        ];
    }

    public static function getTypeSkan($value)
    {
        $types = self::typesSkan();
        if (isset($types[$value])) {
            return $types[$value];
        }
        return '';
    }
}
