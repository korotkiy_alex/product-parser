<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TyreParser') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>
<body class="tp-login-background">

<main class="d-flex">
    <div class="container mt-5 mb-auto">
        <div class="align-content-center flex-row">
            <section class="tp-login-text-container text-center my-auto mx-auto">
                <p class="tp-login-logo">
                    <b>Tyre</b>Parser
                </p>
                <p>An innovative way of trade tyres</p>
            </section>
            <section>
                <div class="panel panel-default box-login mx-auto">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <input id="username" type="text" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus class="@if ($errors->has('username')) is-invalid @endif">
                            @if ($errors->has('username'))
                                <span class="help-block text-center text-danger">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif

                            <input id="password" type="password" name="password" placeholder="Password" required class="@if ($errors->has('password')) is-invalid @endif">
                            @if ($errors->has('password'))
                                <span class="help-block text-center text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    Remember Me
                                </label>
                            </div>

                            <button type="submit">
                                Login
                            </button>

                            <a class="btn btn-link pull-right" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>
<footer>
    <p>&copy;2019 TyreParser. Made for a better life</p>
</footer>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>
