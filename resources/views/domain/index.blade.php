@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h3>Домены</h3>
        <hr>
        <div class="row">
            <div class="card border-0 rounded-0 w-100">
                <div class="card-body">
                    <table class="table table-sm table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th>ID</th>
                                <th>Название</th>
                                <th>Кол. ссылок</th>
                                <th>Кол. связей</th>
                                <th>Карта сайта</th>
                                <th>Тип скана</th>
                                <th>Регулярное выражение</th>
                                <th>Путь к прайсу</th>
                                <th>Дата парсинга</th>
                                <th>Статус</th>
                                <th>Загрузка</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($domains as $domain)
                            <tr>
                                <td>{{$domain->id}}</td>
                                <td>{{$domain->name}}</td>
                                <td></td>
                                <td></td>
                                <td class="text-center">
                                    @if ($domain->have_sitemap) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif
                                </td>
                                <td>{{App\Domain::getTypeSkan($domain->type_skan)}}</td>
                                <td class="text-center">
                                    @if ($domain->regular_price_query) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif
                                </td>
                                <td class="text-center">
                                    @if ($domain->price_file_url) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="action-column edit"><a href="{{route('domains.edit', $domain->id)}}"><i class="fas fa-pencil-alt"></i></a></td>
                                <td class="action-column remove">
                                    <form action="{{ route('domains.destroy', $domain->id) }}" method="@method('DESTROY')">
                                        {{ csrf_field() }}
                                        <button type="submit"><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer d-flex justify-content-center">
                    {!! $domains->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection