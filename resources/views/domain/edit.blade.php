@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h5>Редактирование домена {{$domain->name}}</h5>
        <hr>
        <div class="row">
            <div class="card border-0 rounded-0 w-100">
                <div class="card-body">
                    <form action="{{route('domains.update', $domain->id)}}" method="@method('PUT')">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="name">Название</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="name" name="name" value="{{$domain->name}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="keywords">Ключевые слова</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="keywords" name="keywords" value="{{$domain->keywords}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="sitemap_url">Адрес карты сайта</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="sitemap_url" name="sitemap_url" value="{{$domain->sitemap_url}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="regular_price_query">Регулярное выражение</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="regular_price_query" name="regular_price_query" value="{{$domain->regular_price_query}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="price_file_url">Путь к прайсу</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="price_file_url" name="price_file_url" value="{{$domain->price_file_url}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="relation_file_url">Путь к связям</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="relation_file_url" name="relation_file_url" value="{{$domain->relation_file_url}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="type_skan">Тип сканирования</label>
                            <div class="col-sm-9">
                                <select class="form-control rounded-0" id="type_skan" name="type_skan">
                                    @foreach(\App\Domain::typesSkan() as $key => $value)
                                        <option value="{{$key}}" @if ($domain->type_skan == $key) selected @endif>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="created_at">Добавлен</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="created_at" value="{{$domain->created_at}}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="date_skan">Дата парсинга</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="date_skan" value="{{$domain->date_skan}}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-nowrap" for="status">Статус</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control rounded-0" id="status" value="{{$domain->status}}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary rounded-0">Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection