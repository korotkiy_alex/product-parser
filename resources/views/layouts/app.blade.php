<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>

<div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
        <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar" class="sidebar-wrapper">
        <div class="sidebar-content">
            <div class="sidebar-brand">
                <a href="#">tyre parser</a>
                <div id="close-sidebar">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="sidebar-header">
                <div class="user-pic">
                    <img class="img-responsive img-rounded"
                         src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
                         alt="User picture">
                </div>
                <div class="user-info">
                    <span class="user-name">{{Auth::user()->username}}</span>
                    <span class="user-role">Администратор</span>
                    <span class="user-status">
                        <i class="fa fa-circle"></i>
                        <span>Online</span>
                    </span>
                </div>
            </div>
            <!-- sidebar-header  -->
            <div class="sidebar-search">
                <div>
                    <div class="input-group">
                        <input type="text" class="form-control search-menu" placeholder="Поиск...">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sidebar-search  -->
            <div class="sidebar-menu">
                <ul>
                    <li class="header-menu">
                        <span>Основные</span>
                    </li>
                    {{--<li class="sidebar-dropdown">--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-tachometer-alt"></i>--}}
                    {{--<span>Dashboard</span>--}}
                    {{--</a>--}}
                    {{--<div class="sidebar-submenu">--}}
                    {{--<ul>--}}
                    {{--<li>--}}
                    {{--<a href="#">Dashboard 1</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="#">Dashboard 2</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="#">Dashboard 3</a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</li>--}}
                    <li class="@if (Route::currentRouteName() == 'dashboard') active @endif">
                        <a href="{{route('dashboard')}}">
                            <i class="fa fa-tachometer-alt"></i>
                            <span>Главная</span>
                        </a>
                    </li>
                    <li class="@if (Route::currentRouteName() == 'domains.index') active @endif">
                        <a href="{{route('domains.index')}}">
                            <i class="fa fa-folder"></i>
                            <span>Домены</span>
                        </a>
                    </li>
                    <li class="@if (Route::currentRouteName() == 'links.index') active @endif">
                        <a href="{{route('links.index')}}">
                            <i class="fa fa-book"></i>
                            <span>Ссылки</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- sidebar-menu  -->
        </div>
        <!-- sidebar-content  -->
        <div class="sidebar-footer">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">Выйти</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </nav>
    <!-- sidebar-wrapper  -->
    <main class="page-content">
        @yield('content')
    </main>
    <!-- page-content" -->
</div>

<!-- Scripts -->
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/sidebar.js') }}"></script>
</body>
</html>
