<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->boolean('have_sitemap');
            $table->string('sitemap_url', 255);
            $table->unsignedSmallInteger('status')->default(0);
            $table->unsignedSmallInteger('type_skan');
            $table->dateTime('date_scan')->nullable();
            $table->text('keywords');
            $table->text('regular_price_query');
            $table->string('price_file_url', 255)->nullable();
            $table->string('relation_file_url', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains');
    }
}